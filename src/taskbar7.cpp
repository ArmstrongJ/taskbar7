#include <shobjidl.h>

#define EXPORT __declspec( dllexport )

static DWORD tlsId = 0;

extern "C" {

void EXPORT taskbar7_init()
{
    HRESULT res;
        ITaskbarList3 *g_interface;
    
    tlsId = TlsAlloc();
    
    g_interface = (ITaskbarList3*) TlsGetValue(tlsId);

    res = CoInitialize(NULL);
        
    if((res != S_OK) && (res != S_FALSE))
        return;

    if(CoCreateInstance(CLSID_TaskbarList, NULL, CLSCTX_INPROC_SERVER, IID_ITaskbarList3, (void**)&g_interface) == S_OK)
    {
        if(g_interface->HrInit() == S_OK)
        {
            TlsSetValue(tlsId, g_interface);
            return;
        }
        else
        {
            g_interface->Release();
            g_interface = NULL;
            CoUninitialize();
            return;
        }
    }
    else
    {
        g_interface = NULL;
        CoUninitialize();
        return;
    }
}

void EXPORT taskbar7_uninit()
{
    ITaskbarList3 *g_interface = (ITaskbarList3*) TlsGetValue(tlsId);
    
    if(!g_interface)
        return;

    g_interface->Release();
    g_interface = NULL;
    TlsSetValue(tlsId, NULL);
    CoUninitialize();

    return;
}

int EXPORT taskbar7_state(HWND hwnd, int flags)
{
    ITaskbarList3 *g_interface;

    if(TlsGetValue(tlsId) == 0)
        taskbar7_init();

    g_interface = (ITaskbarList3*) TlsGetValue(tlsId);
    
    if(!g_interface)
        return 0;

    g_interface->SetProgressState(hwnd, (TBPFLAG) flags);
    return 1;
}

int EXPORT taskbar7_progress(HWND hwnd, unsigned int complete, unsigned int total)
{
    ITaskbarList3 *g_interface = (ITaskbarList3*) TlsGetValue(tlsId);

    if(!g_interface)
        return 0;

    if(g_interface->SetProgressValue(hwnd, static_cast<ULONGLONG>(complete), static_cast<ULONGLONG>(total)) == S_OK)
        return 1;
    else
        return 0;
}

}
